# Notes

These are personal notes because I'll forget everything I typed here in about
two hours.

## Setup

Created a `kubernetes` cluster on GCP using:

```sh
gcloud config set project aqua-teen
gcloud config set compute/zone us-west1-a
gcloud container clusters create k0
gcloud container clusers get-credentials k0
```

## Build

All services can be built using the `Makefile` by running `make` at the root of
the project.

## Docker

Building `docker` images and pushing to the GitLab registry is done with the
commands:

```sh
make image
# eg. for jerry
docker push registry.gitlab.com/geoff.jay/aqua/jerry:1.0.0
```

## Running

```sh
make container
```

## Kubernetes

Commands that were used to run the deployment.

```sh
kubectl create -f k8s/deployments/jerry.yaml
kubectl get pods
kubectl describe pods
kubectl port-forward jerry-<tab-tab> 10080:80
```

### Hasura

```sh
gcloud sql instances create aqua-teen-postgres --database-version POSTGRES_9_6 --cpu 1 --memory 3840MiB --region us-west1 --project aqua-teen
source .env
gcloud sql users set-password postgres --instance aqua-teen-postgres --password $DB_PASSWORD --project aqua-teen
kubectl create secret generic cloudsql-instance-credentials --from-file=credentials.json=$JSON_KEY_FILE_PATH
kubectl create secret generic cloudsql-db-credentials --from-literal=username=postgres --from-literal=password=$DB_PASSWORD
wget https://raw.githubusercontent.com/hasura/graphql-engine/master/install-manifests/google-cloud-k8s-sql/deployment.yaml
mv deployment.yaml k8s/deployments/hasura.yaml
gcloud sql instances describe aqua-teen-postgres --format="value(connectionName)" --project aqua-teen
vim k8s/deployments/hasura.yaml
# change the value it says to in the file comments
kubectl apply -f k8s/deployments/hasura.yaml
kubectl get pods
kubectl expose deploy/hasura --port 80 --target-port 8080 --type LoadBalancer
kubectl get service
```
