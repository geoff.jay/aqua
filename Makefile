PROJECT=aqua
REGISTRY=registry.gitlab.com/geoff.jay/$(PROJECT)
VERSION=1.0.0

all: build

prepare:
	@mkdir -p app/bin

build: prepare
	@go build -o app/bin/jerry ./app/jerry

build-static: prepare
	@go build --tags netgo \
		--ldflags '-extldflags "-lm -lstdc++ -static"' \
		-o app/bin/jerry ./app/jerry

image: build-static
	@docker build -t $(REGISTRY)/jerry:$(VERSION) -f ./app/jerry/Dockerfile .

container: image
	@docker run -p 10030:80 -p 10031:81 -d $(REGISTRY)/jerry:$(VERSION)

clean:
	@rm -rf app/bin/

.PHONY: all build build-static clean container image prepare
