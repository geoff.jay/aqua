package handlers

import (
	"encoding/json"
	"net/http"
)

type JerryResponse struct {
	Message string `json:"message"`
}

func JerryHandler(w http.ResponseWriter, r *http.Request) {
	response := JerryResponse{
		Message: "Jerry",
	}
	json.NewEncoder(w).Encode(response)
	return
}
