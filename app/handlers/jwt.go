package handlers

import (
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
)

func JWTAuthHandler(h http.HandlerFunc) http.HandlerFunc {
	keyfunc := func(*jwt.Token) (interface{}, error) {
		return []byte("secret"), nil
	}

	return func(w http.ResponseWriter, r *http.Request) {
		token, err := request.ParseFromRequestWithClaims(r, request.AuthorizationHeaderExtractor, jwt.MapClaims{}, keyfunc)
		if err != nil || !token.Valid {
			http.Error(w, "authorization failed", http.StatusUnauthorized)
			return
		}
		h(w, r)
	}
}
